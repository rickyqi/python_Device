# -*- coding: utf-8 -*-  
# Date: 2018/5/25
# Created by rikcy
# ----------程序的功能说明--------------
# 这个小程序只能使用于Cisoc设备，而且远程必须使用ssh登陆
# //远程的enable密码必须关闭，不然会失败//
# 命令的执行可以有两个选择方案：
# 1、新建一个文件专门存方命令集合，一行一条（推荐）
# 2、就在这个文档里面单独一条一条的执行
# ------------程序执行说明--------------
# 对于设备数量多，而且配置一样时，可使用这个小程序统一进行配置
# 比如同时几十台设备同时划分VLAN；
# 有多少台设备直接如下面模板填写即可，会执行循环；
# ----------------说明结束------------------------

from netmiko import ConnectHandler
import netmiko
import time
import os


# 第一台设备
iosv_l2_s1 = {
	'device_type': 'cisco_ios',
	'ip' :'192.168.255.254',
	'username': 'admin',
	'password': 'admin',
	'secret': 'cisco',		#//enable 密码
	'port': '22',		#//ssh端口号
}

# 第二台设备
# iosv_l2_s2 = {
	# 'device_type': 'cisco_ios',
	# 'ip' :'192.168.255.254',
	# 'username': 'admin',
	# 'password': 'admin',
	# 'secret': 'cisco',		#//enable 密码
	# 'port': '22',		#//ssh端口号
# }

# 第三台设备
	# 'device_type': 'cisco_ios',
	# 'ip' :'192.168.255.254',
	# 'username': 'admin',
	# 'password': 'admin',
	# 'secret': 'cisco',		#//enable 密码
	# 'port': '22',		#//ssh端口号
# }

# 存放配置命令的文档
with open ('iosv_l2_config') as f:
	lines = f.read().splitlines()
print (lines)

all_devices = [iosv_l2_s1,]

print ('>>>>>>>>>>>>>开始<<<<<<<<<<<<<<<<<<')

for devices in all_devices:
	net_connect = ConnectHandler(**devices)
	output = net_connect.send_config_set (lines)
	# 这条命令发送的是命令合集。
	# output = net_connect.send_command("show ip int br")
	# 这条可以使用命令一条一条的发；有多少命令重复多少次

	# print ("\n>>>>>>>>> Device {0} <<<<<<<<<") .format(devices['device_type'])
	# 上面这条，需要在python2里面执行
	print (output)
	print (">>>>>>>>>>>>>end<<<<<<<<<<<<")
