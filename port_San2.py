import telnetlib
import threading

'''
多线程端口扫描程序
'''

def get_ip_status(ip,port):
    server = telnetlib.Telnet()
    try:
        server.open(ip,port)
        print('{0} port {1} 开启'.format(ip, port))
    except Exception as err:
        print('{0} port {1} --关闭'.format(ip,port))
    finally:
        server.close()

if __name__ == '__main__':
    # host = '10.28.194.2'
    host = [
        '10.28.194.2',
        '10.28.203.2',
        '10.28.208.2',
       ] 
    for ip in host:
        threads = []
        for port in range(22,24):
            t = threading.Thread(target=get_ip_status,args=(ip,port))
            t.start()
            threads.append(t)

    for t in threads:
        t.join()