import socket

'''
端口扫描小程序，不推荐使用，单线程速度慢。
'''

def get_ip_status(ip,port):
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        server.connect((ip,port))
        print('{0} port {1} 开启'.format(ip, port))
    except Exception as err:
        print('{0} port {1} --关闭'.format(ip,port))
    finally:
        server.close()

if __name__ == '__main__':
    host = [

        '10.28.194.1',
        '10.28.203.1',
        '10.28.208.254',
       ]

    for ip in host:
        for port in range(20,24):
            get_ip_status(ip,port)