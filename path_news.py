import os
import datetime
'''
这些小代码实现的功能是
列出当前目录下面最新的文件名，包括修改和创建的，只读取最新的一个
'''
def new_file(testdir):
    
    #列出目录下所有的文件
    list = os.listdir(testdir)
    #对文件修改时间进行升序排列,后面的'\\'+fn 参数是因为前面 testdir返回的路径后面没有加\\所以这里手动拼接上去
    list.sort(key=lambda fn:os.path.getmtime(testdir+'\\'+fn))
    #获取最新修改时间的文件
    # filetime = datetime.datetime.fromtimestamp(os.path.getmtime(testdir+'\\'+list[-1]))
    #获取文件所在目录
    filepath = os.path.join(testdir,list[-1])
    print("最新修改的文件(夹)："+list[-1])
    # print("时间："+filetime.strftime('%Y-%m-%d %H-%M-%S'))
    return filepath
#返回最新文件或文件夹名：
path = os.getcwd()
print(new_file(path))
