# -*- coding: utf-8 -*-  
from netmiko import ConnectHandler
import netmiko
import time
from datetime import datetime
import os
import csv


print('这是一个成功的登陆脚本')

def main():
	Red = csv.reader(open('ip.csv', 'r'))
	# print(Red)
	host_list = []
	for item in Red:
		# print(item)
		host_list.append(item)
		pass
	# print (host_list)

	for device in host_list:
		# print(device)
		Host = '{}'.format(device[0])
		# print('设备IP：', Host)
		Username = '{}'.format(device[1])
		# print('登陆用户名：', Username)
		Password = '{}'.format(device[2])
		# print('登陆密码：', Password)
		iosv_l2_s1 = {'device_type': 'cisco_ios','ip' :'{}'.format(device[0]),'username':'{}'.format(device[1]) ,'password': '{}'.format(device[2]), 'port': '22' }
		# print(iosv_l2_s1)
		time.sleep(1)

		# return iosv_l2_s1


		all_devices = [iosv_l2_s1]
		start_time = datetime.now()
		print('开始时间：', start_time)

		print ('>>>>>>>>>>>>>开始相关命令输出<<<<<<<<<<<<<<<<<<')
		print(all_devices)

		#下面这两条命令，是把所有配置文件放到（iosv_l2_config.txt）这个文件里面，一行一条命令，可以一直接执行
		with open ('iosv_l2_config.txt') as f:
			lines = f.read().splitlines()


		for devices in all_devices:
			net_connect = ConnectHandler(**devices)
			output = net_connect.send_config_set (lines)
			# 上面这条命令发送的是命令合集。也可以使用下面命令一条一条的发；
			# output = net_connect.send_command("show run")
			print (output)
			#下面是获取到设备的名称（hostname）
			find_hostname = net_connect.find_prompt()
			#下面是获取到设备名称后多了一个# 号，把这个#号进行替换
			hostname = find_hostname.replace('#',"")
			print ('设备名称：', hostname)
			time.sleep(1)

		
			#获取当前时间，（本地电脑时间）
			timez = (time.strftime("%Y-%m-%d_%H-%M-%S", time.localtime()))
			# print(timez)
			#存放配置文件的路径和文件名（设备名+IP＋时间）
			path = 'E:/PJ/python_Device/config/{}_{}_{}.py'.format(hostname, device[0], timez)
			# print(path)
			with open(path,"w") as f:
				read_data = f.write(output)
				f.close()


			# output = net_connect.send_command("cisco")

			# print ("\n>>>>>>>>> 设备类型： {} <<<<<<<<<".format(iosv_l2_s1['device_type']))
			# 上面这条，需要在python2里面执行
			end_time = datetime.now()
			print ("结束时间：", end_time)





main()
# print(xxx)