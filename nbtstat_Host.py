# -*- coding: utf-8 -*-
import csv
import os 
import re
import time
import socket
import sys

# ========================================================


ip_add = []
ip_csv = csv.reader(open('nbtstat.csv', 'r'))
for each in ip_csv:
	'''从CSV文件循环读取每个IP地址，并存到列表ip_add当中'''
	ip_add.append(each[0])

def Get_HostName(ip):
	for each in ip:
		ping_cmd = 'ping ' + each
		Ping_info = os.popen(ping_cmd).readlines()
		# print(Ping_info) #ping测试主机联通性
		TTL = r'TTL=([0-9]{2})'
		TTL_Str = re.findall(TTL, Ping_info[2])
		#z抓取TTL值
		if len(TTL_Str) == 0:
			print('主机Ping不通：', each)
		else:
			# print(type(each))
			HostName = socket.gethostbyname(each)    #获取主机IP地址
			Host_Info = socket.gethostbyaddr(each)[0]		#获取主机的 主机名
			print('主机IP地址：', HostName)
			print('主机名：', Host_Info)

		#下面是获取MAC地址

		Mac_cmd = 'nbtstat -A ' + each

		# print(Mac_cmd)
		Mac_Info = os.popen(Mac_cmd).readlines()
		# print('Mac_Info=', Mac_Info)

		Mac_Info_Sum = ''
		for each in range(0,len(Mac_Info)):
			Mac_Info_Sum = Mac_Info_Sum + Mac_Info[each]
			# print('Mac_Info_Sum1=', Mac_Info_Sum)
			
		Patt_Mac = r'= (.{2}-.{2}-.{2}-.{2}-.{2}-.{2})' #抓取MAC格式进行匹配
		Mac_Addr = re.findall(Patt_Mac, Mac_Info_Sum)
		print('主机MAC地址=', Mac_Addr)
		print('=====================================')

		path = 'config\\jiexie_Host.csv'
		with open(path, 'a', newline='') as f:
			fb = csv.writer(f)
			# fb.writerow(['主机IP', '主机名', 'MAC地址'])	#列的名称
			fb.writerows([[HostName,Host_Info,Mac_Addr]])	#写入的每行数据


Get_HostName(ip_add)