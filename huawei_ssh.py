from netmiko import ConnectHandler
import time

'''
华为交换机相关的简单配置检查与命令输入，还写的不好，后期改进当中
'''

def huawei(ip):
    "华为交换机配置导出"
    huawei_switch = {
        'device_type': 'huawei', #设备类型
        'ip': ip, #设备 IP
        'username': 'user', #登陆用户名
        'password': 'password', #登陆密码
        'port': 22, #登陆端口,默认 22
        # 'secret': 'enable', #enable 密码‘’Cisco使用到
        # 'verbose': False,#是否详细报告,默认否
    }
    # print(type(huawei_switch))
    print("ip=",ip)
    try:
        net_connect = ConnectHandler(**huawei_switch) #连接到交换机        

        # output = net_connect.send_command("dis int br | in up")
        # print(output)
        config_commands = [ 'dis ip int br',
                    'dis int br | in up',]
        for cmd in config_commands:         #对命令进行遍历，输入
            output = net_connect.send_command(cmd)

            path ='./txt.config.py'             #文件保存路径
            print("output=",output)
            with open(path, "a") as f:          # a 可以对这个文件进行增量添加
                read_data = f.write(output)
                f.close()
    except Exception as e:
        #把登陆不了的IP地址写入到Err_ip.py这个文件当中
        print("连接错误,可尝试在交换机输入这条命令：ssh user admin authentication-type password\n错误IP地址：", ip)
        path2='./Err_ip.py'
        with open(path2, "a") as e:
            Err_data = e.write(ip)
            e.close()
    else:
        time.sleep(1)

if __name__ == '__main__':
    ips = [

    '10.28.198.1',
    '10.28.195.1',
    '10.28.211.1',
    '10.28.189.1',
    '10.28.193.1',
    '10.28.204.1',

    ]
    for ip in ips:
        print(huawei(ip))
