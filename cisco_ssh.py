# -*- coding: utf-8 -*-  
# Date: 2018/5/25
# Created by rikcy
# ----------程序的功能说明--------------
# 这个小程序只能使用于Cisoc设备，而且远程必须使用ssh登陆
# 远程的enable可以开启，会自动进行登陆
# 命令的执行可以有两个选择方案：
# 1、新建一个文件专门存方命令集合，一行一条（推荐）
# 2、就在这个文档里面单独一条一条的执行
# ------------程序执行说明--------------
# 对于设备数量多，而且配置一样时，可使用这个小程序统一进行配置
# 比如同时几十台设备同时划分VLAN；
# 有多少台设备直接如下面模板填写即可，会执行循环；
# ----------------说明结束------------------------

from netmiko import ConnectHandler
import netmiko
import time
import os
import csv


# 第一台设备
iosv_l2_s1 = {'device_type': 'cisco_ios','ip' :'10.6.144.1','username': 'admin','password': 'vanke@Admin', 'port': '22' }

# iosv_l2_s1 = {
# 	'device_type': 'cisco_ios',
# 	'ip' :'10.6.144.1',
# 	'username': 'admin',
# 	'password': 'vanke@Admin',
# 	# 'secret': 'cisco',		#//enable 密码
# 	'port': '22',		#//ssh端口号
# }
# print(type(iosv_l2_s1))

# 第二台设备
# iosv_l2_s2 = {
	# 'device_type': 'cisco_ios',
	# 'ip' :'192.168.255.254',
	# 'username': 'admin',
	# 'password': 'admin',
	# 'secret': 'cisco',		#//enable 密码
	# 'port': '22',		#//ssh端口号
# }

# 第三台设备
	# 'device_type': 'cisco_ios',
	# 'ip' :'192.168.255.254',
	# 'username': 'admin',
	# 'password': 'admin',
	# 'secret': 'cisco',		#//enable 密码
	# 'port': '22',		#//ssh端口号
# }

with open ('iosv_l2_config.txt') as f:	
	lines = f.read().splitlines()
# print (lines)

all_devices = [iosv_l2_s1,]

print ('>>>>>>>>>>>>>开始相关命令输出<<<<<<<<<<<<<<<<<<')
# print(all_devices)

for devices in all_devices:
	net_connect = ConnectHandler(**devices)
	# output = net_connect.send_config_set (lines)
	# 这条命令发送的是命令合集。也可以使用命令一条一条的发；
	output = net_connect.send_command("show run")
	# print (output)
	hostname = net_connect.send_command("show run | in hostname")
	#下面这条命令是获取主机名称，做为后期保存配置文件名使用
	fileName = hostname.split(' ')[-1] #进行主机名的分割
	print('设备名称：', fileName)
	#获取当前时间，（本地电脑时间）
	time = (time.strftime("%Y-%m-%d_%H-%M-%S", time.localtime()))
	# print(time)
	#存放配置文件的路径
	path = 'D:/PJ/python_Device/config/{}_{}.py'.format(fileName, time)
	# print(path)
	with open(path,"w") as f:
		read_data = f.write(output)
		f.close()


	# output = net_connect.send_command("cisco")



	# print ("\n>>>>>>>>> Device {0} <<<<<<<<<") .format(devices['device_type'])
	# 上面这条，需要在python2里面执行
	
	print (">>>>>>>>>>>>>end<<<<<<<<<<<<")
