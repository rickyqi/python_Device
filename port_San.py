import telnetlib
import threading
import queue

'''
这个小程序主要用来扫描相关服务器或者设备的端口开启状态
'''
 
def get_ip_status(ip):
    server = telnetlib.Telnet()
    for port in range(22,23):       #扫描端口
        try:
            server.open(ip,port)
            print('{0} port {1} 开启'.format(ip, port))
        except Exception as err:
            print('{0} port {1} --关闭'.format(ip,port))
        finally:
            server.close()
 
def check_open(q):
    try:
        while True:
            ip = q.get_nowait()
            get_ip_status(ip)
    except queue.Empty as e:
        pass
 
if __name__ == '__main__':
    host = [

            '10.28.194.2',
            '10.28.203.2',
            '10.28.208.2',
           ]     # 这里填写需要扫描的IP地址，列表模式
    q = queue.Queue()
    for ip in host:
        q.put(ip)

    threads = []
    for i in range(10):
        t = threading.Thread(target=check_open,args=(q,))
        t.start()
        threads.append(t)
 
    for t in threads:
        t.join()