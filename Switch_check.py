# coding=utf-8
#2018-9-23
import subprocess
import shlex
#=====端口扫描====
import telnetlib
#==========华为==========
from netmiko import ConnectHandler
import time
import datetime

#======snmp===========
import netsnmp
import re

starttime = datetime.datetime.now()
#=================设备ping测试==========================
Ping_Pass = []
Ping_Fail = []
def icmp(ip):
        cmd = "ping -c 1 -w 2 " + ip  # -c ping 包  -w 是超时时间
        # print("ip=",cmd)
        args = shlex.split(cmd)
        try:
                subprocess.check_call(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                # print ("ping", ip,"通")
                Ping_Pass.append(ip)
        except subprocess.CalledProcessError:
                # print ("ping", ip,"不通")
                Ping_Fail.append(ip)
        # print("ping通=", Ping_Pass)
        # print("ping不通=", Ping_Fail)
        pass

#====================端口扫描======================
ssh_port_open = []
ssh_port_close = []
def get_ip_status(ip,port):
        server = telnetlib.Telnet()
        try:
                server.open(ip,port)
                ssh_port_open.append(ip)
        except Exception as err:
                ssh_port_close.append(ip)
        finally:
                server.close()
        # print("开启=",ssh_port_open)
        # print("关闭=",ssh_port_close)
        pass
#=================设备SNMP获取==========================
Huawei_Snmp =  []
Cisco_Snmp = []
Error_Snmp = []
def Run_Snmp(ip):
        vars = netsnmp.VarList(netsnmp.Varbind('ipAdEntAddr.{}'.format(ip)),netsnmp.Varbind('sysDescr.0'))
        res = ip, netsnmp.snmpget(*vars.varbinds, Version=2, DestHost=ip, Community='Vankero@0755')
        Huawei = re.search(r'\bHuawei\b', str(res), re.M|re.I|re.U)
        Cisco = re.search(r'\bIOS\b', str(res), re.M|re.I|re.U)

        # --》假设不是华为设备 --》那就是Cisco 设备 ---》都不是 ---》就是华为设备
        #Huawei ==None: 表示不是华为设备
        try:
                if Huawei ==None:
                        # print("不是华为设备=",ip)
                        if Cisco !=None:
                                # print('思科SNMP信息输出:', Cisco[0], ip)
                                Cisco_Snmp.append(ip)
                        else:
                                # print("不是华为也不是思科", ip)
                                Error_Snmp.append(ip)
                else:
                        # print('华为SNMP信息输出:', Huawei[0],ip)
                        Huawei_Snmp.append(ip)
        except:
                print("SNMP信息获取失败")
        pass

#===============华为交换机配置======================
def huawei(ip):
        "华为交换机配置导出"
        huawei_switch = {
                'device_type': 'huawei', #设备类型
                'ip': ip, #设备 IP
                'username': 'admin', #登陆用户名
                'password': 'password', #登陆密码
                'port': 22, #登陆端口,默认 22
        }
        try:
                net_connect = ConnectHandler(**huawei_switch) #连接到交换机
                # output = net_connect.send_command("dis int br | in up")
                # print(output)
                config_commands = [ 'dis ip int br',
                                    'dis int br | in up',]
                for cmd in config_commands:         #对命令进行遍历，输入
                        output = net_connect.send_command(cmd)

                        path ='./huawei_Check_config.py'             #文件保存路径
                        print("output=",output)
                        with open (path, "a") as f:          # a 可以对这个文件进行增量添加
                                read_data = f.write(output)
                                f.close()
        except:
                print(type(ip))

                #把登陆不了的IP地址写入到huawei_ssh_login_Fail.py这个文件当中
                print("连接错误,可尝试在交换机输入这条命令：ssh user admin authentication-type password\n错误IP地址：", ip)
                path2='./huawei_ssh_login_Fail.py'
                with open(path2, "a") as e:
                        Err_data = e.write(ip)
                        e.close()
        finally:
                time.sleep(1)


#===============思科交换机配置======================
def Cisco(ip):
        "华为交换机配置导出"
        cisco_switch = {
                'device_type': 'cisco_ios',  # 设备类型
                'ip': ip,  # 设备 IP
                'username': 'admin',  # 登陆用户名
                'password': 'password',  # 登陆密码
                'port': 22,  # 登陆端口,默认 22
        }
        try:
                net_connect = ConnectHandler(**cisco_switch)
                config_commands = ['sh ip int br | in up',
                                   'sh lld nei',
                                   'sh run',
                                   ]
                for cmd in config_commands:  # 对命令进行遍历，输入
                        output = net_connect.send_command(cmd)
                        print(output)

                        path = './cisco_Check_config.py'
                        with open(path, "a") as f:
                                read_data = f.write(output)
                                f.close()
        except ValueError:
                print('参数错误ip:',ip)
                path2 = './cisco_ssh_login_Fail.py'
                with open(path2, "a") as e:
                        Err_data = e.write(ip)
                        e.close()
        finally:
                time.sleep(1)



if __name__ == '__main__':
        # 这里填写需要扫描的IP地址，列表模式
        host = [
                '10.6.141.1',
                '10.6.144.1',
                '10.6.148.1',
                '10.6.148.285',
        ]
        for ip in host:
                icmp(ip)
#====================端口扫描遍历===================
        for ip in Ping_Pass:
                get_ip_status(ip,22)


#====================SNMP======================
        for ip in ssh_port_open:
                Run_Snmp(ip)

        print("#====================ssh连接===============")
        for sshd in Huawei_Snmp:
                huawei(sshd)
                print("华为sshd=", sshd)

        for sshd in Cisco_Snmp:
                Cisco(sshd)
                print("思科sshd=", sshd)


        print("#=============信息打印======================")

        print("ping通的IP地址=",Ping_Pass)
        print("Ping不通的IP地址=",Ping_Fail, "\n")

        print("SSH_22端口打开IP地址=",ssh_port_open)
        print("SSH_22端口关闭IP地址：=",ssh_port_close, "\n")

        print("华为设备SNMP信息=",Huawei_Snmp)
        print("思科设备SNMP信息=",Cisco_Snmp)
        print("SNMP信息获取失败IP=",Error_Snmp, "\n")

        print("#=========相关输出文件==================")
        print("华为设备配置检查文件：huawei_Check_config.py")
        print("华为设备检查失败记录：huawei_ssh_login_Fail.py", "\n")

        print("思科设备配置检查文件：cisco_Check_config.py")
        print("思科设备检查失败记录：cisco_ssh_login_Fail.py", "\n")

        endtime = datetime.datetime.now()
        print("完工，总耗时：", (endtime - starttime).seconds,"秒")




